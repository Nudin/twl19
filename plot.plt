#!/usr/bin/env gnuplot
set datafile separator "\t"

set terminal png size 1400,1600
set output 'entwicklung.png'

set key autotitle columnhead noenhanced
set key outside;
set key bottom center;

set xtics auto
set xtic rotate by -90
plot	 for [i=3:14]	'entwicklung.txt'	u 1:i:xtic(2) w lp ps 0.3
unset output


set terminal png size 1400,1400
set output 'entwicklung-top.png'
plot	 'entwicklung.txt'	u 1:5:xtic(2) w lp ps 0.3, \
		 'entwicklung.txt'	u 1:7:xtic(2) w lp ps 0.3, \
		 'entwicklung.txt'	u 1:8:xtic(2) w lp ps 0.3, \
		 'entwicklung.txt'	u 1:12:xtic(2) w lp ps 0.3
unset output
