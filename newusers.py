#!/usr/bin/env python3

import requests

addr = "https://de.wikipedia.org/w/api.php"
opts = {
    "action": "query",
    "format": "json",
    "list": "logevents",
    "leprop": "timestamp|title",
    "letype": "newusers",
    "lestart": "2019-06-17T00:00:08.000Z",
    "ledir": "newer",
    "lelimit": "max",
}

newusers = []

req = requests.get(addr, params=opts)
res = req.json()
newusers += [event.get("title") for event in res["query"]["logevents"]]

while "continue" in res:
    opts["continue"] = res["continue"]["continue"]
    opts["lecontinue"] = res["continue"]["lecontinue"]
    req = requests.get(addr, params=opts)
    res = req.json()
    newusers += [event.get("title") for event in res["query"]["logevents"]]
    print(".", end="")

newusers = list(filter(None, newusers))

print("")
print(f"found {len(newusers)} new users")
with open("newusers", "w") as f:
    f.write("\n".join(newusers))
