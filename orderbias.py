#!/usr/bin/env python3

import datetime
import re

import dateutil.parser
import requests

addr = "https://de.wikipedia.org/w/api.php"
getrevsopts = {
    "action": "query",
    "format": "json",
    "prop": "revisions",
    "titles": "Wikipedia:Umfragen/Technische Wünsche 2019 Themenschwerpunkte",
    "rvprop": "ids|timestamp",
    "rvlimit": "max",
    "rvstartid": "189614997",
    "rvendid": "190011915",
    "rvdir": "newer",
}
getcontentopts = {
    "action": "query",
    "format": "json",
    "prop": "revisions",
    "titles": "Wikipedia:Umfragen/Technische Wünsche 2019 Themenschwerpunkte",
    "rvprop": "content|timestamp",
}

req = requests.get(addr, params=getrevsopts)
res = req.json()
revisions = {
    event.get("revid"): event.get("timestamp")
    for event in res["query"]["pages"]["10836845"]["revisions"]
}

counter = [0] * 13
topiccounter = {
    "Abstimmungen auf Wikipedia leichter durchführen können": 0,
    "Außenstehenden besser vermitteln, wie Wikipedia funktioniert": 0,
    "Bessere Unterstützung von Geoinformationen": 0,
    "Bessere Unterstützung von Nicht-Text-Formaten": 0,
    "Einfach einheitliche Typografie in Artikeln erzeugen": 0,
    "Fehler bei der Arbeit mit Belegen reduzieren": 0,
    "Hilfestellung beim Bearbeiten von Artikeln": 0,
    "Inhalte leichter vor Vandalismus schützen": 0,
    "Leichter finden, was ich suche": 0,
    "Leichter mit Vorlagen arbeiten": 0,
    "Mediendateien leichter entdecken": 0,
    "Unterschiedliche Wikis einheitlicher bearbeiten können": 0,
    "Von Inhaltsänderungen erfahren, die mich interessieren": 0,
}

popularity = {
    "Leichter mit Vorlagen arbeiten": 2.50420168067227,
    "Bessere Unterstützung von Geoinformationen": 2.46218487394958,
    "Einfach einheitliche Typografie in Artikeln erzeugen": 2.36134453781513,
    "Fehler bei der Arbeit mit Belegen reduzieren": 2.26050420168067,
    "Außenstehenden besser vermitteln, wie Wikipedia funktioniert": 1.8655462184874,
    "Inhalte leichter vor Vandalismus schützen": 1.67226890756303,
    "Bessere Unterstützung von Nicht-Text-Formaten": 1.39495798319328,
    "Mediendateien leichter entdecken": 1.35294117647059,
    "Unterschiedliche Wikis einheitlicher bearbeiten können": 1.34453781512605,
    "Von Inhaltsänderungen erfahren, die mich interessieren": 1.15966386554622,
    "Leichter finden, was ich suche": 1.05042016806723,
    "Abstimmungen auf Wikipedia leichter durchführen können": 1.04201680672269,
    "Hilfestellung beim Bearbeiten von Artikeln": 1,
}

prevdate = None
prevstate = []
for revid in revisions:
    params = getcontentopts.copy()
    params["rvstartid"] = revid
    params["rvendid"] = revid
    req = requests.get(addr, params=params)
    res = req.json()
    text = res["query"]["pages"]["10836845"]["revisions"][0]["*"]

    date = dateutil.parser.parse(revisions[revid])
    if prevdate and prevstate:
        timedelta = date - prevdate
        print(date)
        print(timedelta)
        for num, topic in enumerate(prevstate):
            print(topic)
            params = getcontentopts.copy()
            params["titles"] += "/" + topic
            params["rvlimit"] = 1
            params["rvdir"] = "older"
            params["rvstart"] = date.isoformat()
            req = requests.get(addr, params=params)
            res = req.json()
            pages = res["query"]["pages"]
            text = list(pages.values())[0]["revisions"][0]["*"]
            ts = list(pages.values())[0]["revisions"][0]["timestamp"]
            votes = len(re.findall(r"^\* ", text, flags=re.M))
            counter[num] += (votes - topiccounter[topic]) / popularity[topic]
            topiccounter[topic] = votes

    prevdate = date
    prevstate = re.findall(r"^\{\{/(.*)\}\}$", text, flags=re.M)

print(counter)
