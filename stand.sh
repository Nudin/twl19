#!/bin/bash

baseurl="https://de.wikipedia.org/w/api.php?action=query&format=json&prop=revisions&rvprop=content&rvlimit=1&titles=Wikipedia:Umfragen/Technische_Wünsche_2019_Themenschwerpunkte%2F"

areas=("Abstimmungen_auf_Wikipedia_leichter_durchführen_können" \
"Außenstehenden_besser_vermitteln,_wie_Wikipedia_funktioniert" \
"Bessere_Unterstützung_von_Geoinformationen" \
"Bessere_Unterstützung_von_Nicht-Text-Formaten" \
"Einfach_einheitliche_Typografie_in_Artikeln_erzeugen" \
"Fehler_bei_der_Arbeit_mit_Belegen_reduzieren" \
"Hilfestellung_beim_Bearbeiten_von_Artikeln" \
"Inhalte_leichter_vor_Vandalismus_schützen" \
"Leichter_finden,_was_ich_suche" \
"Leichter_mit_Vorlagen_arbeiten" \
"Mediendateien_leichter_entdecken" \
"Unterschiedliche_Wikis_einheitlicher_bearbeiten_können" \
"Von_Inhaltsänderungen_erfahren,_die_mich_interessieren")

nl="
"

function getvalues() {
	totalusers=""
	for area in "${areas[@]}"; do
		votes=$(curl -s "${baseurl}${area}"  | jq -r '.query .pages | .[] .revisions | .[0] ."*"' | \
			grep -oi '\(Benutzer\|Benutzerin\|User\):[^]|]*' | sort | uniq )
		echo -e "$votes" | wc -l | tr -d '\n'
		echo -e "\t$area"
		totalusers="$totalusers$nl$votes"
	done
	echo -en "Total number of voters: "
	echo -e "$totalusers" | grep -oi '\(Benutzer\|Benutzerin\|User\):[^]|]*' | sort | uniq -c | tee abstimmende | wc -l
}

getvalues | sort -nr | tr _ \  #| tee /data/project/twl17/public_html/2019.txt

