#!/bin/bash

baseurl="https://de.wikipedia.org/w/api.php?action=query&format=json&prop=revisions&rvprop=content&rvlimit=1&titles=Wikipedia:Umfragen/Technische_Wünsche_2019_Themenschwerpunkte%2F"

baseurl="https://de.wikipedia.org/w/api.php?action=query&format=json&prop=revisions&rvprop=timestamp%7Ccontent&rvlimit=1&rvdir=newer&titles=Wikipedia%3AUmfragen%2FTechnische%20W%C3%BCnsche%202019%20Themenschwerpunkte%2F"

areas=("Abstimmungen_auf_Wikipedia_leichter_durchführen_können" \
"Außenstehenden_besser_vermitteln,_wie_Wikipedia_funktioniert" \
"Bessere_Unterstützung_von_Geoinformationen" \
"Bessere_Unterstützung_von_Nicht-Text-Formaten" \
"Einfach_einheitliche_Typografie_in_Artikeln_erzeugen" \
"Fehler_bei_der_Arbeit_mit_Belegen_reduzieren" \
"Hilfestellung_beim_Bearbeiten_von_Artikeln" \
"Inhalte_leichter_vor_Vandalismus_schützen" \
"Leichter_finden,_was_ich_suche" \
"Leichter_mit_Vorlagen_arbeiten" \
"Mediendateien_leichter_entdecken" \
"Unterschiedliche_Wikis_einheitlicher_bearbeiten_können" \
"Von_Inhaltsänderungen_erfahren,_die_mich_interessieren")

echo -en "-\t"
for area in "${areas[@]}"; do
	echo -en "${area//_/ }\t"
done
echo

hours=0
res=1
declare -A lastgood
for day in {17..30}; do
	if [ $day -eq 17 ]; then
		starttime=09
	else
		starttime=00
	fi
	for hour in $(eval echo {$starttime..23..$res}); do
		echo -en "$hours\t"
		((hours+=res))
		if [ $((${hour#0}%6)) -eq 0 ]; then
			echo -en "${day}.Jun-${hour}:00\t"
		else
			echo -en "\t"
		fi
		for area in "${areas[@]}"; do
			votes=$(curl -s "${baseurl}${area}&rvstart=2019-06-${day}T${hour}%3A00%3A00.000Z"  \
				| jq -r '.query .pages | .[] .revisions | .[0] ."*"' | grep -e 'Benutzer' -e 'User' | uniq)
			if [ "$votes"  == "" ]; then
				echo -en "${lastgood[$area]}"
			else
				nrvotes=$(echo -en "$votes" | wc -l )
				lastgood[$area]=$nrvotes
				echo -n "$nrvotes"
			fi
			echo -en '\t'
		done
		echo
	done
done
